package com.epam.studentproject.dao;

import com.epam.studentproject.entity.Tour;
import com.epam.studentproject.entity.Type;

import javax.sql.DataSource;
import java.sql.Date;
import java.time.ZonedDateTime;
import java.util.List;

public interface TourDao {

    List<Tour> getToursByActive(boolean isActive);

    Tour getTourById(long id);

    boolean updateTour(long id, Tour tour);

    boolean deleteTourById(long id);

    long createTour(Tour tour);

    List<Tour> getAllTours();

    List<Tour> getTourByFromDate(Date date);

    List<Tour> getToursByHot(boolean isHot);

    List<Tour> getToursByPrice(double price);

    List<Tour> getToursByMaxAmountOfPeople(int maxAmountOfPeople);

    List<Tour> getToursByPlace(String place);
}
