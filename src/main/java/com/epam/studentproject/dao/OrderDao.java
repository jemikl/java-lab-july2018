package com.epam.studentproject.dao;

import com.epam.studentproject.entity.Order;
import java.sql.Date;
import java.util.List;

public interface OrderDao {

    long createOrder(Order order);
    boolean deleteOrder(long orderId);
    boolean updateOrder(long orderId, Order newOrder);
    List<Order> getAllOrders();
    Order getOrderById(long id);
    List<Order> getOrdersByCustomerId(long userId);
    List<Order> getOrdersByTourId(long tourId);
    List<Order> getOrdersByDate(Date date);
    List<Order> getOrderByAgentId(long agentId);
}
