package com.epam.studentproject.dao;

import com.epam.studentproject.entity.Type;

import java.util.List;

public interface TypeDao {
    public List<Type> getAllTypes();
}