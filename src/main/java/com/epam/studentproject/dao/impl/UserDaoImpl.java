package com.epam.studentproject.dao.impl;

import com.epam.studentproject.dao.UserDao;
import com.epam.studentproject.entity.Role;
import com.epam.studentproject.entity.User;
import com.epam.studentproject.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
@Slf4j
public class UserDaoImpl implements UserDao {
    private final static String ADD_USER_QUERY = "INSERT INTO user (first_name, last_name, id_user_role, is_regular, email, telephone, password) VALUES (?,?,?,?,?,?,?)";
    private final static String DELETE_USER_QUERY = "DELETE FROM user " +
            "WHERE id_user = ?";
    private final static String UPDATE_USER_QUERY = "UPDATE user SET first_name = ?, last_name = ?, id_user_role = ?, is_regular = ?, email = ?, telephone = ?, password = ? " +
            "WHERE id_user = ?";
    private final static String UPDATE_USER_WITHOUT_PASSWORD_QUERY = "UPDATE user SET first_name = ?, last_name = ?, id_user_role = ?, is_regular = ?, email = ?, telephone = ?" +
            "WHERE id_user = ?";
    private final static String GET_ALL_USERS_QUERY = "SELECT * FROM user " +
            "INNER JOIN role " +
            "ON user.id_user_role = role.id_user_role";
    private final static String GET_USERS_BY_FULL_NAME_QUERY = "SELECT * " +
            "FROM user " +
            "INNER JOIN role " +
            "on user.id_user_role = role.id_user_role " +
            "WHERE first_name = ? AND last_name = ?";
    private final static String GET_USER_BY_ID_QUERY = "SELECT * " +
            "FROM user " +
            "INNER JOIN role " +
            "on user.id_user_role = role.id_user_role " +
            "WHERE id_user = ?";
    private final static String GET_USER_BY_MAIL_QUERY = "SELECT * " +
            "FROM user " +
            "INNER JOIN role " +
            "on user.id_user_role = role.id_user_role " +
            "WHERE email = ?";


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long addUser(User user) {
        String name = user.getFirstName();
        String lastName = user.getLastName();

        Role role = user.getRole();
        boolean isRegular = user.isRegular();
        String email = user.getEmail();
        String tel = user.getTelephone();

        String password = user.getPassword();

        final PreparedStatementCreator psc = connection -> {
            final PreparedStatement ps = connection.prepareStatement(ADD_USER_QUERY, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, name);
            ps.setString(2, lastName);
            ps.setInt(3, role.getUserRoleId());
            ps.setBoolean(4, isRegular);
            ps.setString(5, email);
            ps.setString(6, tel);
            ps.setString(7, password);
            return ps;
        };

        final KeyHolder holder = new GeneratedKeyHolder();

        int jdbcTemplateAnswer = jdbcTemplate.update(psc, holder);

        final long newId = holder.getKey().longValue();

        if (jdbcTemplateAnswer == 1) {
            log.info("User created. User id: {}", newId);
        } else {
            log.error("User did not created");
        }
        log.info("add user with id = {}", newId);
        return newId;
    }

    @Override
    public boolean deleteUser(long id) {
        int jdbcTemplateAnswer = jdbcTemplate.update(DELETE_USER_QUERY, id);

        boolean isUserDeleted = jdbcTemplateAnswer == 1;

        if (isUserDeleted) {
            log.info("User successfully deleted");
        } else {
            log.error("User did not deleted");
        }

        return isUserDeleted;
    }

    @Override
    public boolean updateUser(long id, User userNew) {
        String name = userNew.getFirstName();
        String lastName = userNew.getLastName();

        Role role = userNew.getRole();
        boolean isRegular = userNew.isRegular();
        String email = userNew.getEmail();
        String tel = userNew.getTelephone();

        String password = userNew.getPassword();

        int jdbcTemplateAnswer = jdbcTemplate.update(UPDATE_USER_QUERY, name, lastName, role.getUserRoleId(), isRegular, email, tel, password, id);

        boolean isUserUpdated = jdbcTemplateAnswer == 1;

        if (isUserUpdated) {
            log.info("User successfully updated");
        } else {
            log.error("User did not updated");
        }

        return isUserUpdated;
    }

    @Override
    public boolean updateUserWithoutPassword(long id, User userNew) {
        String name = userNew.getFirstName();
        String lastName = userNew.getLastName();

        Role role = userNew.getRole();
        boolean isRegular = userNew.isRegular();
        String email = userNew.getEmail();
        String tel = userNew.getTelephone();

        int jdbcTemplateAnswer = jdbcTemplate.update(UPDATE_USER_WITHOUT_PASSWORD_QUERY, name, lastName, role.getUserRoleId(), isRegular, email, tel, id);

        boolean isUserUpdated = jdbcTemplateAnswer == 1;

        if (isUserUpdated) {
            log.info("User successfully updated");
        } else {
            log.error("User did not updated");
        }

        return isUserUpdated;
    }

    @Override
    public List<User> getAllUsers() {
        List users = jdbcTemplate.query(GET_ALL_USERS_QUERY, new UserMapper());
        return users;
    }

    @Override
    public List<User> getUsersByFullName(String firstName, String lastName) {
        List users = jdbcTemplate.query(GET_USERS_BY_FULL_NAME_QUERY, new Object[]{firstName, lastName}, new UserMapper());
        return users;
    }

    @Override
    public User getUserById(long id) {
        List<User> users = jdbcTemplate.query(GET_USER_BY_ID_QUERY, new Object[]{id}, new UserMapper());
        if (users.size() == 1) {
            return users.get(0);
        } else {
            return null;
        }
    }

    @Override
    public User getUserByEmail(String email) {
        List<User> user = jdbcTemplate.query(GET_USER_BY_MAIL_QUERY, new Object[]{email}, new UserMapper());
        return user.get(0);
    }

}
