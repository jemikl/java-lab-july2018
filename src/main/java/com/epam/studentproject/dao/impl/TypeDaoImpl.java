package com.epam.studentproject.dao.impl;

import com.epam.studentproject.dao.TypeDao;
import com.epam.studentproject.entity.Type;
import com.epam.studentproject.mapper.TypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TypeDaoImpl implements TypeDao {

    public static String GET_ALL_TYPES = "SELECT * " +
            "FROM type";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TypeDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Type> getAllTypes() {
        return jdbcTemplate.query(GET_ALL_TYPES, new TypeMapper());
    }
}
