package com.epam.studentproject.dao.impl;

import com.epam.studentproject.dao.OrderDao;
import com.epam.studentproject.entity.Order;
import com.epam.studentproject.mapper.OrderMapper;
import com.mysql.jdbc.Statement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Slf4j
@Repository
public class OrderDaoImpl implements OrderDao {

    private static final String ADD_ORDER_QUERY = "INSERT INTO orders (id_user, id_tour, order_date, amount_of_people, final_price) VALUES (?,?,?,?,?)";
    private static final String DELETE_ORDER_QUERY = "DELETE FROM orders WHERE id_order = ?";
    private static final String UPDATE_ORDER_QUERY = "UPDATE orders SET id_tour = ?, id_user = ?, order_date = ?, amount_of_people = ?, final_price = ? WHERE id_order = ?";
    private static final String SELECT_ALL_ORDERS_QUERY = "SELECT * FROM orders";
    private static final String SELECT_ORDER_BY_ID_QUERY = "SELECT * FROM orders WHERE id_order = ?";
    private static final String SELECT_ORDERS_BY_CUSTOMER_ID_QUERY = "SELECT * FROM orders WHERE id_user = ?";
    private static final String SELECT_ORDERS_BY_TOUR_ID_QUERY = "SELECT * FROM orders WHERE id_tour = ?";
    private static final String SELECT_ORDERS_BY_DATE_QUERY = "SELECT * FROM orders WHERE order_date = ?";
    private static final String SELECT_ORDERS_BY_AGENT_ID = "SELECT * FROM orders INNER JOIN tour ON orders.ID_TOUR = tour.ID_TOUR WHERE tour.id_user = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public long createOrder(Order order) {
        KeyHolder key = new GeneratedKeyHolder();
        final PreparedStatementCreator ps = connection -> {
            java.sql.PreparedStatement _ps = connection.prepareStatement(ADD_ORDER_QUERY, Statement.RETURN_GENERATED_KEYS);
            _ps.setLong(1, order.getUserId());
            _ps.setLong(2, order.getTourId());
            _ps.setDate(3, order.getDate());
            _ps.setInt(4, order.getAmountOfPeople());
            _ps.setDouble(5, order.getFinalPrice());
            return _ps;
        };
        int numberOfAffectedRows = jdbcTemplate.update(ps, key);
        long orderId = numberOfAffectedRows == 1 ? key.getKey().longValue() : -1;
        log.info("Order created. Order id: {}", orderId);
        return orderId;
    }

    @Override
    public boolean deleteOrder(long orderId) {
        int numberOfAffectedRows = jdbcTemplate.update(DELETE_ORDER_QUERY, orderId);
        boolean isOrderDeleted = numberOfAffectedRows == 1;

        if (isOrderDeleted) {
            log.info("Order successfully deleted");
        } else {
            log.error("Order did not deleted");
        }

        return isOrderDeleted;
    }

    @Override
    public boolean updateOrder(long orderId, Order newOrder) {
        int numberOfAffectedRows = jdbcTemplate.update(UPDATE_ORDER_QUERY, newOrder.getTourId(), newOrder.getUserId(), newOrder.getDate(), newOrder.getAmountOfPeople(), newOrder.getFinalPrice(), orderId);

        boolean isOrderUpdated = numberOfAffectedRows != 0;

        if (isOrderUpdated) {
            log.info("Order successfully updated");
        } else {
            log.error("Order did not updated");
        }

        return isOrderUpdated;
    }

    @Override
    public List<Order> getAllOrders() {
        return jdbcTemplate.query(SELECT_ALL_ORDERS_QUERY, new OrderMapper());
    }

    @Override
    public Order getOrderById(long id) {
        return jdbcTemplate.queryForObject(SELECT_ORDER_BY_ID_QUERY, new Object[]{id}, new OrderMapper());
    }

    @Override
    public List<Order> getOrdersByCustomerId(long customerId) {
        return jdbcTemplate.query(SELECT_ORDERS_BY_CUSTOMER_ID_QUERY, new Object[]{customerId}, new OrderMapper());
    }

    @Override
    public List<Order> getOrdersByTourId(long tourId) {
        return jdbcTemplate.query(SELECT_ORDERS_BY_TOUR_ID_QUERY, new Object[]{tourId}, new OrderMapper());
    }

    @Override
    public List<Order> getOrdersByDate(Date date) {
        return jdbcTemplate.query(SELECT_ORDERS_BY_DATE_QUERY, new Object[]{date}, new OrderMapper());
    }

    @Override
    public List<Order> getOrderByAgentId(long agentId) {
        return jdbcTemplate.query(SELECT_ORDERS_BY_AGENT_ID, new Object[]{agentId}, new OrderMapper());
    }
}
