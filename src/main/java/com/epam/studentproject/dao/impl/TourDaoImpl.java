package com.epam.studentproject.dao.impl;

import com.epam.studentproject.dao.TourDao;
import com.epam.studentproject.entity.Tour;
import com.epam.studentproject.mapper.TourMapper;
import com.mysql.jdbc.Statement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
@Slf4j
public class TourDaoImpl implements TourDao {

    private static final String GET_TOURS_BY_ACTIVE = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE WHERE tour.IS_ACTIVE = ?";
    private static final String GET_TOUR_BY_ID = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE WHERE ID_TOUR = ?";
    private static final String UPDATE_TOUR = "UPDATE tour SET id_tour_type = ?, is_active = ?," +
            " from_date = ?, duration = ?, is_hot = ?," +
            "hot_discount = ?, discount = ?, price = ?," +
            "max_amount_of_people = ?, place = ?, description = ?" +
            " WHERE id_tour = ?";

    private static final String DELETE_TOUR_BY_ID = "DELETE FROM tour WHERE id_tour = ?";
    private static final String CREATE_TOUR = "INSERT INTO tour " +
            "(id_user, id_tour_type, is_active, from_date, duration, is_hot," +
            "hot_discount, discount, price, max_amount_of_people, place, description)" +
            " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String GET_WITH_INACTIVE = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE";
    private static final String GET_BY_FROM_DATE = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE WHERE tour.FROM_DATE >= ?";
    private static final String GET_BY_HOT = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE WHERE tour.IS_HOT = ?";
    private static final String GET_BY_PRICE = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE WHERE tour.PRICE <= ?";
    private static final String GET_BY_MAX_AMOUNT_OF_PEOPLE = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE WHERE tour.MAX_AMOUNT_OF_PEOPLE <= ?";
    private static final String GET_BY_PLACE = "SELECT * FROM tour INNER JOIN type ON tour.ID_TOUR_TYPE = type.ID_TOUR_TYPE WHERE tour.PLACE = ?";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TourDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Tour> getToursByActive(boolean isActive) {
        int isActiveInDB = 0;
        if (isActive) {
            isActiveInDB = 1;
        }
        return jdbcTemplate.query(GET_TOURS_BY_ACTIVE, new Object[]{isActiveInDB}, new TourMapper());
    }

    @Override
    public Tour getTourById(long id) {
        return jdbcTemplate.queryForObject(GET_TOUR_BY_ID, new Object[]{id}, new TourMapper());
    }

    @Override
    public boolean updateTour(long id, Tour tour) {
        int rowCount = jdbcTemplate.update(UPDATE_TOUR, tour.getType().getTourTypeId(), tour.isActive(), tour.getFromDate(),
                tour.getDuration(), tour.isHot(), tour.getHotDiscount(), tour.getDiscount(), tour.getPrice(),
                tour.getMaxAmountOfPeople(), tour.getPlace(), tour.getDescription(), id);

        boolean isTourUpdated = rowCount == 1;

        if (isTourUpdated) {
            log.info("Tour [id = {}] updated successfully", id);
        } else {
            log.error("Tour [id = {}] did not updated", id);
        }

        return isTourUpdated;
    }

    @Override
    public boolean deleteTourById(long id) {
        int rowCount = jdbcTemplate.update(DELETE_TOUR_BY_ID, id);

        boolean isTourDeleted = rowCount == 1;

        if (isTourDeleted) {
            log.info("Tour deleted successfully");
        } else {
            log.error("Tour did not updated");
        }

        return isTourDeleted;
    }

    @Override
    public long createTour(Tour tour) {
        KeyHolder key = new GeneratedKeyHolder();
        final PreparedStatementCreator ps = connection -> {
            java.sql.PreparedStatement _ps = connection.prepareStatement(CREATE_TOUR, Statement.RETURN_GENERATED_KEYS);
            _ps.setLong(1, tour.getUserId());
            _ps.setLong(2, tour.getType().getTourTypeId());
            _ps.setBoolean(3, tour.isActive());
            _ps.setDate(4, tour.getFromDate());
            _ps.setLong(5, tour.getDuration());
            _ps.setBoolean(6, tour.isHot());
            _ps.setDouble(7, tour.getHotDiscount());
            _ps.setDouble(8, tour.getDiscount());
            _ps.setDouble(9, tour.getPrice());
            _ps.setInt(10, tour.getMaxAmountOfPeople());
            _ps.setString(11, tour.getPlace());
            _ps.setString(12, tour.getDescription());
            return _ps;
        };

        int numberOfAffectedRows = jdbcTemplate.update(ps, key);

        long tourId = numberOfAffectedRows == 1 ? key.getKey().longValue() : -1;

        log.info("Tour created. Tour id: {}", tourId);

        return tourId;
    }

    @Override
    public List<Tour> getAllTours() {
        return jdbcTemplate.query(GET_WITH_INACTIVE, new TourMapper());
    }

    @Override
    public List<Tour> getTourByFromDate(Date date) {
        return jdbcTemplate.query(GET_BY_FROM_DATE, new Object[]{date}, new TourMapper());
    }

    @Override
    public List<Tour> getToursByHot(boolean isHot) {
        return jdbcTemplate.query(GET_BY_HOT, new Object[]{isHot}, new TourMapper());
    }


    @Override
    public List<Tour> getToursByPrice(double price) {
        return jdbcTemplate.query(GET_BY_PRICE, new Object[]{price}, new TourMapper());
    }

    @Override
    public List<Tour> getToursByMaxAmountOfPeople(int maxAmountOfPeople) {
        return jdbcTemplate.query(GET_BY_MAX_AMOUNT_OF_PEOPLE, new Object[]{maxAmountOfPeople}, new TourMapper());
    }

    @Override
    public List<Tour> getToursByPlace(String place) {
        return jdbcTemplate.query(GET_BY_PLACE, new Object[]{place}, new TourMapper());
    }
}
