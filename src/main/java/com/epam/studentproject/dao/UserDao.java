package com.epam.studentproject.dao;

import com.epam.studentproject.entity.User;

import java.util.List;

public interface UserDao {

    long addUser(User user);

    boolean deleteUser(long id);

    boolean updateUser(long id, User userNew);

    boolean updateUserWithoutPassword(long id, User userNew);

    List<User> getAllUsers();

    List<User> getUsersByFullName(String firstName, String lastName);

    User getUserById(long id);

    User getUserByEmail(String email);
}
