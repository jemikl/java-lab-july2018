package com.epam.studentproject.service;

import com.epam.studentproject.dto.OrderDto;
import com.epam.studentproject.entity.Order;

import java.sql.Date;
import java.util.List;

public interface OrderService {

    Order prepareOrder(Order order);
    long createOrder(Order order) throws Exception;
    boolean deleteOrder(long orderId);

    Order getOrderById(long orderId);
    List<Order> getAllOrders();
    List<Order> getOrdersByDate(Date date);
    List<Order> getOrdersByCustomerId(long customerId);
    List<Order> getOrdersByAgentId(long agentId);

    List<OrderDto> getAllOrdersDto();
    List<OrderDto> getAllAgentsOrdersDto(long agentId);
    List<OrderDto> getAllCustomersOrdersDto(long customerId);



}
