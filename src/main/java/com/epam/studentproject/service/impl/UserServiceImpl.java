package com.epam.studentproject.service.impl;

import com.epam.studentproject.dao.UserDao;
import com.epam.studentproject.entity.User;
import com.epam.studentproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private OrderServiceImpl orderService;

    private static final Pattern emailVerificationPattern = Pattern.compile("^\\w+.?\\w{2,49}+@[a-z]+\\.[a-z]+$");
    private static final Pattern telephoneVerificationPattern = Pattern.compile("^\\+?\\d{1,4}\\d{10}$");

    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @Override
    public long createUser(User user) throws Exception {
        userInfoVerification(user);
        user.setPassword(encoder.encode(user.getPassword())); // password encryption
        return userDao.addUser(user);
    }

    @Override
    public boolean updateUser(long userId, User newInfo) throws Exception {
        passwordValidation(newInfo.getPassword());
        newInfo.setPassword(encoder.encode(newInfo.getPassword())); // password encryption
        return userDao.updateUser(userId, newInfo);
    }

    @Override
    public boolean updateUserWithoutPassword(long userId, User newInfo) throws Exception {
        telephoneValidation(newInfo.getTelephone());
        return userDao.updateUserWithoutPassword(userId, newInfo);
    }

    @Override
    public boolean deleteUser(long userId) {
        return userDao.deleteUser(userId);
    }

    @Override
    public User getUserById(long userId) {
        return userDao.getUserById(userId);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    @Override
    public List<User> getUsersByFullName(String firstName, String lastName) {
        return userDao.getUsersByFullName(firstName, lastName);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public List<User> getUsersByRole(int roleId) {
        List<User> users = new ArrayList<>();
        for (User user : userDao.getAllUsers()) {
            if (user.getRole().getUserRoleId() == roleId) {
                users.add(user);
            }
        }
        return users;
    }

    @Override
    public int getOrdersQuantity(long userId) {
        return orderService.getOrdersByCustomerId(userId).size();
    }

    private void emailValidation(String email) throws Exception {
        if (!emailVerificationPattern.matcher(email).matches()) {
            throw new Exception("Incorrect email!");
        }

        for (User usersInDataBase : userDao.getAllUsers()) {
            if (usersInDataBase.getEmail().equals(email)) {
                throw new Exception("This email is registered already!");
            }
        }
    }

    private void telephoneValidation(String telephone) throws Exception {
        if (!telephoneVerificationPattern.matcher(telephone).matches()) {
            throw new Exception("Incorrect telephone number!");
        }
    }

    private void userInfoVerification(User user) throws Exception {
        if (user.getFirstName().equals("") || user.getLastName().equals("")) {
            throw new Exception("Input your first name and last name!");
        }

        emailValidation(user.getEmail());
        passwordValidation(user.getPassword());
        telephoneValidation(user.getTelephone());
    }

    private void userNewInfoValidation(User user) throws Exception {
        if (user.getFirstName().equals("") || user.getLastName().equals("")) {
            throw new Exception("Input your first name and last name!");
        }

        telephoneValidation(user.getTelephone());
    }

    private void passwordValidation(String password) throws Exception {
        if (password.length() == 0) {
            throw new Exception("Input your password!");
        }

        if (password.length() < 6) {
            throw new Exception("Create a password longer than 5 characters");
        }
    }

    @Override
    public UserDetails loadUserByUsername(@NotNull String email) throws UsernameNotFoundException {
        return Optional.of(userDao.getUserByEmail(email))
                .orElseThrow(() -> new UsernameNotFoundException("User with email '" + email + "' not registered yet!"));
    }
}
