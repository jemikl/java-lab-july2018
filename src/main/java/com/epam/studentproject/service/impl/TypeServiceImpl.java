package com.epam.studentproject.service.impl;

import com.epam.studentproject.dao.TypeDao;
import com.epam.studentproject.entity.Type;
import com.epam.studentproject.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    TypeDao typeDao;

    @Override
    public List<String> getAllTypesNames() {
        List<String> typesNames = new ArrayList<>();

        for (Type type : typeDao.getAllTypes()) {
            typesNames.add(type.getName());
        }

        return typesNames;
    }

    @Override
    public List<Type> getAllTypes() {
        return typeDao.getAllTypes();
    }

    @Override
    public int getIdByTypeName(String typeName) {
        List<Type> types = typeDao.getAllTypes();
        List<Integer> ids = new ArrayList<>();

        for (Type type : types) {
            if (type.getName().equals(typeName)) {
                ids.add(type.getTourTypeId());
            }
        }

        if (ids.size() != 1) {
            throw new RuntimeException();
        }

        return ids.get(0);
    }
}
