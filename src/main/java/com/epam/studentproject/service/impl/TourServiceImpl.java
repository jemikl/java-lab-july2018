package com.epam.studentproject.service.impl;

import com.epam.studentproject.dao.impl.TourDaoImpl;
import com.epam.studentproject.entity.Tour;
import com.epam.studentproject.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class TourServiceImpl implements TourService {
    @Autowired
    private TourDaoImpl tourDao;

    @Override
    public long createTour(Tour tour) {

        if (validationTour(tour)) {
            if (!tour.getFromDate().before((new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(6))))) {
                tour.setActive(true);
            } else {
                tour.setActive(false);
            }
            return tourDao.createTour(tour);
        } else {
            throw new RuntimeException("Illegal tour");
        }
    }

    @Override
    public boolean updateTour(long tourId, Tour tour) {

        if (validationTour(tour)) {
            return tourDao.updateTour(tourId, tour);
        } else {
            throw new RuntimeException("Illegal tour");
        }
    }

    @Override
    public boolean deleteTour(long tourId) {
        return tourDao.deleteTourById(tourId);
    }

    @Override
    public Tour getTourById(long tourId) {
        return tourDao.getTourById(tourId);
    }

    @Override
    public List<Tour> getAllTours() {
        return tourDao.getAllTours();
    }

    @Override
    public List<Tour> getToursByAgentId(long userId) {
        List<Tour> result = new ArrayList<>();
        List<Tour> allTours = tourDao.getAllTours();

        for (Tour tour : allTours) {
            if (tour.getUserId() == userId) {
                result.add(tour);
            }
        }
        return result;
    }

    @Override
    public List<Tour> getToursByActive(boolean isActive) {
        return tourDao.getToursByActive(isActive);
    }

    @Override
    public List<Tour> getToursByHot(boolean isHot) {
        return tourDao.getToursByHot(isHot);
    }

    @Override
    @Scheduled(fixedDelay = 6 * 60 * 60 * 1000)
    public void updateActiveByIncorrectDateAndQuantity() {
        for (Tour tour : tourDao.getAllTours()) {
            if (tour.getFromDate().before((new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(6))))) {
                tour.setActive(false);
                tourDao.updateTour(tour.getTourId(), tour);
            }
            if (tour.getMaxAmountOfPeople() < 1) {
                tour.setActive(false);
                tourDao.updateTour(tour.getTourId(), tour);
            }
        }
    }

    @Override
    public void updateHot(long tourId, boolean isHot) {
        Tour tour = tourDao.getTourById(tourId);
        tour.setHot(isHot);
        tourDao.updateTour(tourId, tour);
    }

    private boolean validationTour(Tour tour) {
        boolean isTourGood = true;
        if (!tour.getFromDate().after(new Date(System.currentTimeMillis()))) {
            System.out.println("bad time");
            isTourGood = false;
        }
        if (!(tour.getDuration() >= 0)) {
            System.out.println("bad duration");
            isTourGood = false;
        }

        if (!(tour.getHotDiscount() >= 0 && tour.getHotDiscount() <= 100)) {
            System.out.println("bad hot discount");
            isTourGood = false;
        }

        if (!(tour.getDiscount() >= 0 && tour.getDiscount() <= 100)) {
            System.out.println("bad discount");
            isTourGood = false;
        }

        if (!(tour.getPrice() > 0)) {
            System.out.println("bad price");
            isTourGood = false;
        }

        if (!(tour.getMaxAmountOfPeople() >= 0)) {
            System.out.println("bad people amount");
            if (tour.getMaxAmountOfPeople() == 0) {
                tour.setActive(false);
            }
            isTourGood = false;
        }

        return isTourGood;
    }

    @Override
    public List<Tour> getAllToursSortedByStartDate() {
        return sortToursByDate(tourDao.getAllTours());
    }

    @Override
    public List<Tour> getAllActiveToursSortedByStartDate() {
        return sortToursByDate(tourDao.getToursByActive(true));
    }

    @Override
    public List<Tour> getAllInactiveToursSortedByStartDate() {
        return sortToursByDate(tourDao.getToursByActive(false));
    }

    private List<Tour> sortToursByDate(List<Tour> tours) {
        tours.sort(((o1, o2) ->
        {
            int result = 0;
            if (o1.getFromDate().before(o2.getFromDate())) {
                result = -1;
            }
            if (o1.getFromDate().after(o2.getFromDate())) {
                result = 1;
            }
            return result;
        }));

        return tours;
    }
}
