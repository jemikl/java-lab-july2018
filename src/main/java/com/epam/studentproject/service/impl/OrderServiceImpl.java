package com.epam.studentproject.service.impl;

import com.epam.studentproject.dao.OrderDao;
import com.epam.studentproject.dto.OrderDto;
import com.epam.studentproject.entity.Order;
import com.epam.studentproject.entity.Tour;
import com.epam.studentproject.entity.User;
import com.epam.studentproject.service.OrderService;
import com.epam.studentproject.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private TourService tourService;

    @Autowired
    private UserServiceImpl userService;


    @Override
    public Order prepareOrder(Order order) {
        Tour tour = tourService.getTourById(order.getTourId());
        User user = userService.getUserById(order.getUserId());

        if (order.getAmountOfPeople() > 0 && order.getDate().before(tour.getFromDate())) {
            order.setFinalPrice(getFinalPrice(order, tour, user));
        }

        return order;
    }

    @Override
    public long createOrder(Order order) throws Exception {
        Tour curTour = tourService.getTourById(order.getTourId());
        User user = userService.getUserById(order.getUserId());

        if(order.getAmountOfPeople() < 0 && order.getAmountOfPeople() > tourService.getTourById(order.getTourId()).getMaxAmountOfPeople()){
            throw new Exception("Illegal amount of people");
        }

        if (order.getAmountOfPeople() > 0 && order.getDate().before(curTour.getFromDate())) {

            curTour.setMaxAmountOfPeople(curTour.getMaxAmountOfPeople() - order.getAmountOfPeople());
            order.setFinalPrice(getFinalPrice(order, curTour, user));

            tourService.updateTour(order.getTourId(), curTour);

            int counterOrders = 0;
            List<Order> orders = orderDao.getAllOrders();

            for (Order order1 : orders) {
                if (user.getUserId() == order1.getUserId()) {
                    counterOrders++;
                }
            }

            if (counterOrders > 4) {
                user.setRegular(true);
                try {
                    userService.updateUserWithoutPassword(user.getUserId(), user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return orderDao.createOrder(order);

        } else {
            throw new RuntimeException("Illegal order");
        }

    }

    private double getFinalPrice(Order order, Tour tour, User user) {
        if (user.isRegular() && tour.isHot()) {
            if (tour.getHotDiscount() > tour.getDiscount()) {
                return order.getAmountOfPeople() * (tour.getPrice() * ((100 - tour.getHotDiscount()) / 100));
            } else {
                return order.getAmountOfPeople() * (tour.getPrice() * ((100 - tour.getDiscount()) / 100));
            }
        } else if (tour.isHot() && !user.isRegular()) {
            return order.getAmountOfPeople() * (tour.getPrice() * ((100 - tour.getHotDiscount()) / 100));
        }
        if (user.isRegular() && !tour.isHot()) {
            return order.getAmountOfPeople() * (tour.getPrice() * ((100 - tour.getDiscount()) / 100));
        } else {
            return  order.getAmountOfPeople() * (tour.getPrice());
        }
    }

    @Override
    public boolean deleteOrder(long orderId) {
        Tour tour = tourService.getTourById(orderDao.getOrderById(orderId).getTourId());
        tour.setMaxAmountOfPeople(tour.getMaxAmountOfPeople() + orderDao.getOrderById(orderId).getAmountOfPeople());
        tourService.updateTour(tour.getTourId(), tour);
        return orderDao.deleteOrder(orderId);
    }

    @Override
    public Order getOrderById(long orderId) {
        return orderDao.getOrderById(orderId);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderDao.getAllOrders();
    }

    @Override
    public List<Order> getOrdersByDate(Date date) {
        return orderDao.getOrdersByDate(date);
    }

    @Override
    public List<Order> getOrdersByCustomerId(long customerId) {
        return orderDao.getOrdersByCustomerId(customerId);
    }

    @Override
    public List<Order> getOrdersByAgentId(long agentId) {
        List<Tour> agentTours = tourService.getToursByAgentId(agentId);
        List<Order> orders = new ArrayList<>();
        for (Tour tour : agentTours) {
            orders.addAll(orderDao.getOrdersByTourId(tour.getTourId()));
        }
        return orders;
    }

    @Override
    public List<OrderDto> getAllOrdersDto() {
        List<Order> orders = orderDao.getAllOrders();
        return ordersToOrdersDto(orders);
    }

    @Override
    public List<OrderDto> getAllAgentsOrdersDto(long agentId) {
        List<Order> orders = orderDao.getOrderByAgentId(agentId);
        return ordersToOrdersDto(orders);
    }

    @Override
    public List<OrderDto> getAllCustomersOrdersDto(long customerId) {
        List<Order> orders = orderDao.getOrdersByCustomerId(customerId);
        return ordersToOrdersDto(orders);
    }

    private List<OrderDto> ordersToOrdersDto(List<Order> orders) {
        List<OrderDto> result = new ArrayList<>();

        for (Order order : orders) {
            OrderDto orderDto = new OrderDto();
            orderDto.setOrderId(order.getOrderId());

            User customer = userService.getUserById(order.getUserId());

            orderDto.setCustomerFirstName(customer.getFirstName());
            orderDto.setCustomerLastName(customer.getLastName());
            orderDto.setCustomerTelephone(customer.getTelephone());


            Tour tour = tourService.getTourById(order.getTourId());
            long agentId = tour.getUserId();
            User agent = userService.getUserById(agentId);
            orderDto.setAgentFirstName(agent.getFirstName());
            orderDto.setAgentLastName(agent.getLastName());
            orderDto.setAgentTelephone(agent.getTelephone());


            orderDto.setDate(order.getDate());
            orderDto.setAmountOfPeople(order.getAmountOfPeople());
            orderDto.setTourPlace(tour.getPlace());
            orderDto.setTourTypeName(tour.getType().getName());

            orderDto.setFinalPrice(order.getFinalPrice());
            orderDto.setToursStartDate(tour.getFromDate());


            result.add(orderDto);
        }

        return result;
    }
}
