package com.epam.studentproject.service;

import com.epam.studentproject.entity.Tour;

import java.util.List;

public interface TourService {

    long createTour(Tour tour);

    boolean updateTour(long tourId, Tour tour);

    boolean deleteTour(long tourId);

    Tour getTourById(long tourId);

    List<Tour> getAllTours();

    List<Tour> getToursByAgentId(long agentId);

    List<Tour> getToursByActive(boolean isActive);

    List<Tour> getToursByHot(boolean isHot);

    void updateActiveByIncorrectDateAndQuantity();

    void updateHot(long tourId, boolean isHot);

    List<Tour> getAllToursSortedByStartDate();

    List<Tour> getAllActiveToursSortedByStartDate();

    List<Tour> getAllInactiveToursSortedByStartDate();

}
