package com.epam.studentproject.service;

import com.epam.studentproject.entity.Type;

import java.util.List;

public interface TypeService {
    public List<String> getAllTypesNames();
    public List<Type> getAllTypes();
    public int getIdByTypeName(String typeName);
}
