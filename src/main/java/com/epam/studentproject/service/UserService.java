package com.epam.studentproject.service;

import com.epam.studentproject.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    long createUser(User user) throws Exception;

    boolean updateUser(long userId, User newInfo) throws Exception;

    boolean updateUserWithoutPassword(long userId, User newInfo) throws Exception;

    boolean deleteUser(long userId);

    User getUserById(long userId);

    User getUserByEmail(String email);

    List<User> getUsersByFullName(String firstName, String lastName);

    List<User> getAllUsers();

    List<User> getUsersByRole(int roleId);

    int getOrdersQuantity(long userId);
}
