package com.epam.studentproject.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private long orderId;
    private String customerFirstName;
    private String customerLastName;
    private String customerTelephone;

    private String agentFirstName;
    private String agentLastName;
    private String agentTelephone;

    private Date date;
    private Date toursStartDate;
    private int amountOfPeople;
    private String tourPlace;
    private String tourTypeName;
    private double finalPrice;
}
