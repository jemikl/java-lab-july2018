package com.epam.studentproject.mapper;

import com.epam.studentproject.entity.Role;
import com.epam.studentproject.entity.User;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setUserId(resultSet.getLong("id_user"));
        user.setRole(new Role(resultSet.getInt("Role.id_user_role"), resultSet.getString("Role.name")));
        user.setRegular(resultSet.getBoolean("is_regular"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setTelephone(resultSet.getString("telephone"));
        return user;
    }
}
