package com.epam.studentproject.mapper;

import com.epam.studentproject.entity.Type;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TypeMapper implements RowMapper<Type> {

    @Override
    public Type mapRow(ResultSet resultSet, int i) throws SQLException {
        Type type = new Type();
        type.setTourTypeId(resultSet.getInt("id_tour_type"));
        type.setName(resultSet.getString("name"));
        return type;
    }
}
