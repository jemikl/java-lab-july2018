package com.epam.studentproject.mapper;

import com.epam.studentproject.entity.Tour;
import com.epam.studentproject.entity.Type;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TourMapper implements RowMapper<Tour> {

    @Override
    public Tour mapRow(ResultSet resultSet, int i) throws SQLException {
        Tour tour = new Tour();
        tour.setTourId(resultSet.getLong("id_tour"));
        tour.setUserId(resultSet.getLong("id_user"));
        tour.setType(new Type(resultSet.getInt("Type.id_tour_type"), resultSet.getString("Type.name")));
        tour.setActive(resultSet.getBoolean("is_active"));
        tour.setFromDate(resultSet.getDate("from_date"));
        tour.setDuration(resultSet.getInt("duration"));
        tour.setHot(resultSet.getBoolean("is_hot"));
        tour.setHotDiscount(resultSet.getDouble("hot_discount"));
        tour.setDiscount(resultSet.getDouble("discount"));
        tour.setPrice(resultSet.getDouble("price"));
        tour.setPlace(resultSet.getString("place"));
        tour.setMaxAmountOfPeople(resultSet.getInt("max_amount_of_people"));
        tour.setDescription(resultSet.getString("description"));
        return tour;
    }
}