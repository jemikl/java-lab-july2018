package com.epam.studentproject.mapper;

import com.epam.studentproject.entity.Role;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleMapper implements RowMapper<Role> {

    @Override
    public Role mapRow(ResultSet resultSet, int i) throws SQLException {
        Role role = new Role();
        role.setUserRoleId(resultSet.getInt("id_user_role"));
        role.setAuthority(resultSet.getString("name"));
        return role;
    }
}
