package com.epam.studentproject.mapper;

import com.epam.studentproject.entity.Order;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderMapper implements RowMapper<Order> {

    @Override
    public Order mapRow(ResultSet resultSet, int i) throws SQLException {
        Order order = new Order();
        order.setOrderId(resultSet.getLong("id_order"));
        order.setTourId(resultSet.getLong("id_tour"));
        order.setUserId(resultSet.getLong("id_user"));
        order.setDate(resultSet.getDate("order_date"));
        order.setAmountOfPeople(resultSet.getInt("amount_of_people"));
        order.setFinalPrice(resultSet.getDouble("final_price"));
        return order;
    }
}
