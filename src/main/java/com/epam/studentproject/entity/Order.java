package com.epam.studentproject.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    private long orderId;
    private long userId;
    private long tourId;
    private Date date;
    private int amountOfPeople;
    private double finalPrice;

    public Order(long userId, long tourId, Date date, int amountOfPeople, double finalPrice) {
        this.userId = userId;
        this.tourId = tourId;
        this.date = date;
        this.amountOfPeople = amountOfPeople;
        this.finalPrice = finalPrice;
    }
}