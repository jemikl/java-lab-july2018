package com.epam.studentproject.entity;

import lombok.*;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tour implements Serializable {

    private long tourId;
    private long userId;
    private Type type;
    private boolean isActive;
    private Date fromDate;
    private int duration;
    private boolean isHot;
    private double hotDiscount;
    private double discount;
    private double price;
    private int maxAmountOfPeople;
    private String place;
    private String description;

    public Tour(long userId, Type type, boolean isActive, Date fromDate, int duration, boolean isHot, double hotDiscount, double discount, double price, int maxAmountOfPeople, String place, String description) {
        this.userId = userId;
        this.type = type;
        this.isActive = isActive;
        this.fromDate = fromDate;
        this.duration = duration;
        this.isHot = isHot;
        this.hotDiscount = hotDiscount;
        this.discount = discount;
        this.price = price;
        this.maxAmountOfPeople = maxAmountOfPeople;
        this.place = place;
        this.description = description;
    }
}



