package com.epam.studentproject.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Type {

    private int tourTypeId;
    private String name;

    public Type(String name) {
        this.name = name;
    }
}
