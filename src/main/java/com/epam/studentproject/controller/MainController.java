package com.epam.studentproject.controller;

import com.epam.studentproject.entity.Role;
import com.epam.studentproject.entity.User;
import com.epam.studentproject.service.TourService;
import com.epam.studentproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private TourService tourService;

    @RequestMapping("/demo")
    public String demo() {
        return "Hello world";
    }

    @RequestMapping(value = {"/", "/home"})
    public String home() {
        return "redirect:active_tours";
    }

    /*@RequestMapping("/error")
    public String error(){
        System.out.println("error");
        return "error";
    }*/

    @RequestMapping("/hello")
    public String hello() {
        System.out.println("hello");
        return "hello";
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("user") User user, Model model) {
        user.setRole(new Role(1, "customer"));
        try {
            if (userService.createUser(user) != -1) {
                model.addAttribute("message", "Registration successful!");
                return "redirect:login";
            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }
        return "registration";
    }
}
