package com.epam.studentproject.controller;

import com.epam.studentproject.dto.OrderDto;
import com.epam.studentproject.entity.Order;
import com.epam.studentproject.service.impl.OrderServiceImpl;
import com.epam.studentproject.service.impl.TourServiceImpl;
import com.epam.studentproject.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Date;
import java.util.List;

@Controller
public class OrderController {

    @Autowired
    OrderServiceImpl orderService;
    @Autowired
    UserServiceImpl userService;
    @Autowired
    TourServiceImpl tourService;

    @RequestMapping("/orders")
    public String allOrders(Model model) {
        List<OrderDto> orderDto = orderService.getAllOrdersDto();
        model.addAttribute("orders", orderDto);
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("tours", tourService.getAllTours());
        return "orders";
    }

    @PostMapping("/customers_orders")
    public String customersOrders(@ModelAttribute("customerId") long customerId, Model model) {
        List<OrderDto> customersOrdersDto = orderService.getAllCustomersOrdersDto(customerId);
        model.addAttribute("orders", customersOrdersDto);
        model.addAttribute("users", userService.getAllUsers());
        return "orders";
    }

    @PostMapping("/agents_orders")
    public String agentsOrders(@ModelAttribute("agentId") long agentId, Model model) {
        List<OrderDto> agentsOrderDto = orderService.getAllAgentsOrdersDto(agentId);
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("orders", agentsOrderDto);
        return "orders";
    }

    @PostMapping("/create_order")
    public String createOrder(@ModelAttribute("tourId") long tourId,
                              @ModelAttribute("amount_of_people") int amount_of_people,
                              @ModelAttribute("userId") long userId, Model model) {

        Order order = new Order();
        order.setUserId(userId);
        order.setTourId(tourId);
        order.setDate(new Date(System.currentTimeMillis()));
        order.setAmountOfPeople(amount_of_people);

        if (amount_of_people > 0) {
            orderService.prepareOrder(order);
            model.addAttribute("order", order);
        } else {
            model.addAttribute("order", order);
        }

        model.addAttribute("user", userService.getUserById(userId));
        model.addAttribute("tour", tourService.getTourById(tourId));
        return "create_order";
    }

    @PostMapping("/create_order_do")
    public String createOrderDo(@ModelAttribute("tourId") long tourId,
                                @ModelAttribute("amount_of_people") int amount_of_people,
                                @ModelAttribute("userId") long userId, Model model) {

        Order order = new Order();
        order.setUserId(userId);
        order.setTourId(tourId);
        order.setDate(new Date(System.currentTimeMillis()));
        order.setAmountOfPeople(amount_of_people);

        try {
            orderService.createOrder(order);
        } catch (Exception e) {
            model.addAttribute("error_message", e.getMessage());
            model.addAttribute("user", userService.getUserById(userId));
            model.addAttribute("tour", tourService.getTourById(tourId));
            return "create_order";
        }

        return "redirect:home";
    }

    @PostMapping("/delete_order")
    public String deleteOrder(@ModelAttribute("orderId") long orderId){
        orderService.deleteOrder(orderId);
        return "redirect:home";
    }


}
