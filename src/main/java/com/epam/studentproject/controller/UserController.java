package com.epam.studentproject.controller;

import com.epam.studentproject.entity.Role;
import com.epam.studentproject.entity.User;
import com.epam.studentproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/users")
    public String users(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        return "users";
    }

    /*@GetMapping("/user_info")
    public String userInfoGet(@ModelAttribute("userId") long userId, Model model) {
        model.addAttribute("user", userService.getUserById(userId));
        model.addAttribute("userId", userId);
        return "user_info";
    }*/

    @PostMapping("/user_info")
    public String userInfoPost(@ModelAttribute("userId") long userId, Model model) {
        model.addAttribute("user", userService.getUserById(userId));
        model.addAttribute("userId", userId);
        return "user_info";
    }

    @GetMapping("/user_info_change")
    public String userInfoChange(@ModelAttribute("userId") long userId, Model model) {
        model.addAttribute("user_old_info", userService.getUserById(userId));
        model.addAttribute("userId", userId);
        return "user_info_change";
    }

    @PostMapping("/user_info_change")
    public String userInfoChange(@ModelAttribute("user") User newUserInfo, @ModelAttribute("userId") long userId, Model model) {
        User oldUserInfo = userService.getUserById(userId);

        newUserInfo.setUserId(userId);
        newUserInfo.setRole(oldUserInfo.getRole());
        newUserInfo.setRegular(oldUserInfo.isRegular());
        newUserInfo.setEmail(oldUserInfo.getEmail());

        try {
            userService.updateUserWithoutPassword(oldUserInfo.getUserId(), newUserInfo);
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("user_old_info", oldUserInfo);
            return "user_info_change";
        }
        model.addAttribute("userId", userId);
        return "user_info";
    }

    @GetMapping("/create_agent")
    public String createAgentGet(Model model) {
        return "create_agent";
    }

    @PostMapping("/create_agent")
    public String createAgentPost(@ModelAttribute("user") User user, Model model) {
        user.setRole(new Role(2, "agent"));
        try {
            if(userService.createUser(user) != -1) {
                model.addAttribute("message", "Agent created successful!");
                return "redirect:users";
            }
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }
        return "create_agent";
    }
}
