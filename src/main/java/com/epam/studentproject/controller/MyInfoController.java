package com.epam.studentproject.controller;

import com.epam.studentproject.entity.User;
import com.epam.studentproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MyInfoController {

    @Autowired
    UserService userService;

    @RequestMapping("/my_info")
    public String myInfo(Model model, HttpServletRequest request) {
        String userEmail = request.getRemoteUser();
        model.addAttribute("my_info", userService.getUserByEmail(userEmail));
        return "my_info";
    }

    @GetMapping("/my_info_change")
    public String myInfoChange(Model model, HttpServletRequest request) {
        String userEmail = request.getRemoteUser();
        model.addAttribute("my_old_info", userService.getUserByEmail(userEmail));
        return "my_info_change";
    }

    @PostMapping("/my_info_change")
    public String myInfoChange(@ModelAttribute("user") User newUserInfo, Model model, HttpServletRequest request) {

        User oldUserInfo = userService.getUserByEmail(request.getRemoteUser());

        newUserInfo.setUserId(oldUserInfo.getUserId());
        newUserInfo.setRole(oldUserInfo.getRole());
        newUserInfo.setRegular(oldUserInfo.isRegular());
        newUserInfo.setEmail(oldUserInfo.getEmail());

        try {
            userService.updateUserWithoutPassword(oldUserInfo.getUserId(), newUserInfo);
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("my_old_info", oldUserInfo);
            return "my_info_change";
        }

        return "redirect:my_info";
    }

    @PostMapping("/change_password")
    private String changePassword(@ModelAttribute("new_password") String newPassword, @ModelAttribute("user_id") long userId, Model model) {
        User user = userService.getUserById(userId);
        user.setPassword(newPassword);
        try {
            userService.updateUser(userId, user);
            model.addAttribute("successfulMessage", "Password was changed successfully");
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        }
        model.addAttribute("my_info", user);
        return "my_info";
    }
}
