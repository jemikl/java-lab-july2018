package com.epam.studentproject.controller;

import com.epam.studentproject.entity.Tour;
import com.epam.studentproject.entity.Type;
import com.epam.studentproject.entity.User;
import com.epam.studentproject.service.TourService;
import com.epam.studentproject.service.TypeService;
import com.epam.studentproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TourController {


    @Autowired
    UserService userService;

    @Autowired
    TourService tourService;

    @Autowired
    TypeService typeService;


    @RequestMapping("/tours")
    public String allTours(Model model) {
        model.addAttribute("tours", tourService.getAllToursSortedByStartDate());
        model.addAttribute("users", userService.getAllUsers());
        return "tours";
    }

    @GetMapping("/add_tour")
    public String addTourPage(Model model) {
        model.addAttribute("types_names", typeService.getAllTypesNames());
        model.addAttribute("types", typeService.getAllTypes());
        return "add_tour";
    }

    @PostMapping("/add_tour")
    public String addTourDo(@ModelAttribute("tour") Tour tour, HttpServletRequest request, Model model) {

        if (Boolean.parseBoolean(request.getParameter("isHot"))) {
            tour.setHot(true);
        } else {
            tour.setHot(false);
        }

        tour.setActive(true);

        tour.setUserId(userService.getUserByEmail(request.getRemoteUser()).getUserId());

        tour.setType(new Type(typeService.getIdByTypeName(tour.getType().getName()), tour.getType().getName()));

        try {
            tourService.createTour(tour);
        } catch (Exception e) {
            model.addAttribute("error_message", e.getMessage());
            return "redirect:add_tour";
        }

        return "redirect:active_tours";
    }

    @GetMapping("/active_tours")
    public String activeToursPage(Model model) {
        tourService.updateActiveByIncorrectDateAndQuantity();
        model.addAttribute("active_tours", tourService.getAllActiveToursSortedByStartDate());
        model.addAttribute("users", userService.getAllUsers());
        return "active_tours";
    }

    @GetMapping("/inactive_tours")
    public String inactiveToursPage(Model model) {
        tourService.updateActiveByIncorrectDateAndQuantity();
        model.addAttribute("inactive_tours", tourService.getAllInactiveToursSortedByStartDate());
        return "inactive_tours";
    }

    @GetMapping("/all_agents_tours")
    public String allAgentsTours(HttpServletRequest request, Model model) {

        User user = userService.getUserByEmail(request.getRemoteUser());
        model.addAttribute("agentsTours", tourService.getToursByAgentId(user.getUserId()));
        model.addAttribute("user", user);


        return "agents_tours";
    }

    @PostMapping("/tour_active_hot_change")
    public String updateTours(@ModelAttribute("tourId") long tourId, HttpServletRequest request, Model model) {
        model.addAttribute("tour", tourService.getTourById(tourId));
        return "tour_active_hot_change";
    }

    @PostMapping("/all_agents_tours")
    public String updateAllAgentsTours(@ModelAttribute("isActive") String isActive,
                                       @ModelAttribute("isHot") String isHot,
                                       @ModelAttribute("tourId") long tourId,
                                       HttpServletRequest request, Model model) {

        System.out.println("point A");
        Tour tour = tourService.getTourById(tourId);

        if(isActive.equals("isActive")){
            tour.setActive(true);
        }else{
            tour.setActive(false);
        }

        if(isHot.equals("isHot")){
            tour.setHot(true);
        }else{
            tour.setHot(false);
        }

        tourService.updateTour(tourId, tour);

        User user = userService.getUserByEmail(request.getRemoteUser());
        model.addAttribute("agentsTours", tourService.getToursByAgentId(user.getUserId()));
        model.addAttribute("user", user);


        return "agents_tours";
    }
}
