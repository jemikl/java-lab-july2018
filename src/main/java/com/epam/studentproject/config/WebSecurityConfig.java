package com.epam.studentproject.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userServiceImpl")
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "/home", "/active_tours"/*, "/error"*/).permitAll()
                .antMatchers("/login", "/registration").anonymous()
                .antMatchers("/customers_orders", "/create_order", "/create_order_do", "/delete_order").hasAuthority("customer")
                .antMatchers("/agent_orders", "/add_tour", "/all_agents_tours", "/tour_active_hot_change").hasAuthority("agent")
                .antMatchers("/tours", "/users", "/user_info", "/user_info_change", "/inactive_tours", "/orders").hasAuthority("admin")
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").successForwardUrl("/home")
                .and()
                .logout().logoutSuccessUrl("/home").permitAll();
//                .and()
//                .exceptionHandling().accessDeniedPage("/error");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }
}