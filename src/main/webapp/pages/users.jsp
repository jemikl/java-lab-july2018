<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.util.Objects" %>
<%@ page import="com.epam.studentproject.entity.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>All users</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid" style="margin: 5px 5px 5px">
    <hr/>
    <form:form action="/logout" method="post">
        <input type="button" class="btn btn-info" value="Back to active tours" onclick="location.href='/active_tours'">
        <input type="button" class="btn btn-info" value="Create agent" onclick="location.href='/create_agent'">
        <input type="submit" class="btn btn-info" value="Log Out" align="center">
    </form:form>
    <hr/>
    <br>

    <h1 style="text-align: center">User list:</h1>
    <table class="table table-striped table-bordered table-info">
        <tr>
            <th>User ID</th>
            <th>Role</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>Is Regular</th>
        </tr>
        <%
            if(!Objects.isNull(request.getAttribute("users"))){
                List<User> users = (List<User>) request.getAttribute("users");
                for (User user : users) {
                    out.println("<tr><td>" + user.getUserId() + "</td>");
                    out.println("<td>" + user.getRole().getAuthority() + "</td>");
                    out.println("<td>" + user.getFirstName() + "</td>");
                    out.println("<td>" + user.getLastName() + "</td>");
                    out.println("<td>" + user.getEmail() + "</td>");
                    out.println("<td>" + user.getTelephone() + "</td>");
                    out.println("<td>" + user.isRegular() + "</td>");
        %>
                    <%--@elvariable id="userId" type="java.lang.Long"--%>
                    <td><form:form action="/user_info" modelAttribute="userId" method="post">
                        <input type="hidden" class="btn btn-info" name="userId" value=<%=user.getUserId()%>>
                        <input type="submit" class="btn btn-info btn-sm" value="info">
                    </form:form></td>
        <%
                    out.println("</tr>");
                }
            }
        %>
    </table>
</div>

</body>
</html>
