<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid" style="text-align: center" align="center">
        <h1>Registration</h1>
        <span style="margin: 10px 10px 10px" style="color:crimson">
        <%
            if(request.getAttribute("errorMessage") != null){
                out.println(request.getAttribute("errorMessage"));
            }
            out.println("<br>");
        %>
        </span>
        <%--@elvariable id="user" type="com.epam.studentproject.entity.User"--%>
        <form:form action="/registration" modelAttribute="user" method="post">
            <table class="table-borderless" align="center">
                <tr>
                    <td><label style="margin: 5px 5px 5px" > First Name: </label></td>
                    <td><input style="margin: 5px 5px 5px" type="text" name="firstName"/></td>
                </tr>
                <tr>
                    <td><label style="margin: 5px 5px 5px" > Last Name: </label></td>
                    <td><input style="margin: 5px 5px 5px" type="text" name="lastName"/></td>
                </tr>
                <tr>
                    <td><label style="margin: 5px 5px 5px" > Email: </label></td>
                    <td><input style="margin: 5px 5px 5px" type="text" name="email"/></td>
                </tr>
                <tr>
                    <td><label style="margin: 5px 5px 5px" > Password: </label></td>
                    <td><input style="margin: 5px 5px 5px" type="password" name="password" required/></td>
                </tr>
                <tr>
                    <td><label style="margin: 5px 5px 5px" > telephone: </label></td>
                    <td><input style="margin: 5px 5px 5px" type="text" name="telephone"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input style="margin: 5px 5px 5px" type="submit" class="btn btn-info" value="Sign Up"/>
                        <input style="margin: 5px 5px 5px" type="button" class="btn btn-info" value="Cancel" onclick='location.href="/login"'/>
                    </td>
                </tr>
            </table>
        </form:form>
    </div>
</body>
</html>