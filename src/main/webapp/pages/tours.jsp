<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.epam.studentproject.entity.Tour" %>
<%@ page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello</title>
    <link href="${pageContext.request.contextPath}/css/hello.css" rel="stylesheet" type="text/css">
</head>
<body>

<%
    if (request.getUserPrincipal() != null && ((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority().equals("agent")) {
        out.println("<input align=\"left\" type=\"button\" value=\"Add tour\" onclick=\"location.href=\'/add_tour\'\">");
    }

    if (request.getUserPrincipal() == null) {
        out.println("<input align=\"right\" type=\"button\" value=\"Sing In\" onclick=\"location.href=\'/login\'\">");
    } else {
        out.print("<h5 align=\"right\">Logged as " + request.getRemoteUser() + "</h5><br>");
%>
<form:form action="/logout" method="post">
    <% out.print("<input align=\"right\" type=\"submit\" value=\"Log Out\">");%>
</form:form>
<%}%>
<br>
<h1 style="text-align: center">All tours:</h1>
<%
    List<Tour> tours = null;
    if (!Objects.isNull(request.getAttribute("tours"))) {
        tours = (List<Tour>) request.getAttribute("tours");
    }
    if (Objects.isNull(tours) || tours.isEmpty()) {
        out.print("No tours yet");
    } else {
        out.println("<table border=\"1\" cellpadding=\"5\" width=\"100%\">");
        out.println("<tr>");
        out.println("<td>Type</td>");
        out.println("<td>Tour from date</td>");
        out.println("<td>Duration</td>");
        out.println("<td>Is hot</td>");
        out.println("<td>Hot discount</td>");
        out.println("<td>Discount for regular buyers</td>");
        out.println("<td>Base price</td>");
        out.println("<td>Max amount of people</td>");
        out.println("<td>Place</td>");
        out.println("<td>Description</td>");
        out.println("<td>More info</td>");
        out.println("</td>");
        for (Tour tour : tours) {
            out.println("<tr>");
            out.println("<td>" + tour.getType().getName() + "</td>");
            out.println("<td>" + tour.getFromDate() + "</td>");
            out.println("<td>" + tour.getDuration() + "</td>");
            out.println("<td>" + tour.isHot() + "</td>");
            out.println("<td>" + tour.getHotDiscount() + "</td>");
            out.println("<td>" + tour.getDiscount() + "</td>");
            out.println("<td>" + tour.getPrice() + "</td>");
            out.println("<td>" + tour.getMaxAmountOfPeople() + "</td>");
            out.println("<td>" + tour.getPlace() + "</td>");
            out.println("<td>" + tour.getDescription() + "</td>");
            out.println("</tr>");
        }
    }
%>

</body>
</html>