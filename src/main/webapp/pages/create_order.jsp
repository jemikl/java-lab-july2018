<%@ page import="com.epam.studentproject.entity.Order" %>
<%@ page import="com.epam.studentproject.entity.Tour" %>
<%@ page import="com.epam.studentproject.entity.User" %>
<%@ page import="java.util.Objects" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Buy</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid" style="text-align: center; margin: 5px 5px 5px">
    <h1>Order details</h1>
    <table class="table table-striped table-bordered table-info">
        <tr>
            <td colspan="3">
                <%
                    User user = new User();
                    if (!Objects.isNull(request.getAttribute("user"))) {
                        user = (User) request.getAttribute("user");
                    }
                %>
                <label>Customers name: <%=user.getFirstName() + " " + user.getLastName()%></label>
            </td>
        </tr>
        <tr>
            <td>
                <%
                    Tour tour = new Tour();
                    if (!Objects.isNull(request.getAttribute("tour"))) {
                        tour = (Tour) request.getAttribute("tour");
                    }
                %>
                <label>Place: <%=tour.getPlace()%>
                </label>
            </td>
            <td colspan="2">
                <label> Description: <br> <%=tour.getDescription()%>
                </label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Beginning of the tour: <%=tour.getFromDate()%></label>
            </td>
            <td>
                <%
                    Order order = new Order();
                    if (!Objects.isNull(request.getAttribute("order"))) {
                        order = (Order) request.getAttribute("order");
                    }
                %>
                Input amount of people
                <%--@elvariable id="tourId" type="java.lang.Long"--%>
                <%--@elvariable id="amount_of_people" type="java.lang.Integer"--%>
                <%--@elvariable id="userId" type="java.lang.Long"--%>
                <form:form action="/create_order" method="post">
                    <input type="hidden" name="tourId" value=<%=tour.getTourId()%>>
                    <input type="text" name="amount_of_people">
                    <input type="hidden" name="userId" value=<%=user.getUserId()%>>
                    <input type="submit" class="btn btn-info" value="Figure final price">
                </form:form>
                <label>Current amount: <%=order.getAmountOfPeople()%></label>
                <br>
                <label>Max amount: <%=tour.getMaxAmountOfPeople()%></label>
                <br>
                <%
                    if (order.getAmountOfPeople() <= 0 || order.getAmountOfPeople() > tour.getMaxAmountOfPeople()) {
                        out.println("<label>Input correct value more than 0 and less then " + tour.getMaxAmountOfPeople() + "</label>");
                    }
                %>
                <br>
            </td>
            <td>
                <label> Final price: <%=order.getFinalPrice()%></label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <%--@elvariable id="tourId" type="java.lang.Long"--%>
                <%--@elvariable id="amount_of_people" type="java.lang.Integer"--%>
                <%--@elvariable id="userId" type="java.lang.Long"--%>
                <form:form action="/create_order_do" method="post">
                    <input type="hidden" name="tourId" value=<%=tour.getTourId()%>>
                    <input type="hidden" name="amount_of_people" value=<%=order.getAmountOfPeople()%>>
                    <input type="hidden" name="userId" value=<%=user.getUserId()%>>
                    <input type="submit" class="btn btn-success" align="right" value="Buy">
                    <input type="button" class="btn btn-info" value="Cancel" onclick="location.href='/active_tours'"
                           align="right">
                </form:form>
            </td>
        </tr>
    </table>
</div>
</body>
</html>