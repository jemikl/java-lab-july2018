<%@ page import="com.epam.studentproject.entity.Type" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tour add</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>

<body>

<div class="container-fluid" align="center">
    <h1>Tour add</h1>

    <%--@elvariable id="tour" type="com.epam.studentproject.entity.Tour"--%>
    <form:form action="/add_tour" modelAttribute="tour" method="POST">
    <table class="table-borderless">
        <tr>
            <td><label style="margin: 5px 5px 5px" >Type*:</label></td>
            <td><select style="margin: 5px 5px 5px" name="type">
                <%
                    List<Type> types = null;


                    if (!Objects.isNull(request.getAttribute("types"))) {
                        types = (List<Type>) request.getAttribute("types");
                    }

                    if (Objects.isNull(types) || types.isEmpty()) {
                        out.println("Default type (not of them exists in data base)");
                    }

                    for (Type type : types) {
                        out.println("<option> " + type.getName() + " </option>");
                    }
                %>
            </select></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Tour Start Date*:</label></td>
            <td><input style="margin: 5px 5px 5px" type="date" min="<%=DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now().plusDays(1))%>" name="fromDate" required></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Tour Duration*:</label></td>
            <td><input style="margin: 5px 5px 5px" type="number" name="duration" required></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Is Tour Hot?</label></td>
            <td><input style="margin: 5px 5px 5px" type="checkbox" name="isHot" value="true"></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Hot Discount*:</label></td>
            <td><input style="margin: 5px 5px 5px" type="number" name="hotDiscount" value="0" required></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Discount For Regular Buyers*:</label></td>
            <td><input style="margin: 5px 5px 5px" type="number" name="discount" value="0" required></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Base Price*:</label></td>
            <td><input style="margin: 5px 5px 5px" type="number" name="price" required><td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Tour Quantity*:</label></td>
            <td><input style="margin: 5px 5px 5px" type="number" name="maxAmountOfPeople" required></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Tour Place*:</label></td>
            <td><input style="margin: 5px 5px 5px" type="text" name="place" required></td>
        </tr>

        <tr>
            <td><label style="margin: 5px 5px 5px" >Tour Description:</label></td>
            <td><input style="margin: 5px 5px 5px" type="text" name="description"></td>
        </tr>
    </table>
        <input style="margin: 5px 5px 5px" type="submit" class="btn btn-info" value="Add tour">
        <input style="margin: 5px 5px 5px" type="button" class="btn btn-info" value="Cancel" onclick='location.href="/active_tours"'>
    </form:form>

    <%
        System.out.println(request.getAttribute("error_message"));
        if (!Objects.isNull(request.getAttribute("error_message"))) {
            out.println("Invalid data: " + request.getAttribute("error_message"));
        }
    %>
</div>

</body>
</html>