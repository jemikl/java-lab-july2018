<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.epam.studentproject.entity.Tour" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My tours</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid" style="margin: 5px 5px 5px">
    <hr/>
    <form:form action="/logout" method="post">
        <input type="button" class="btn btn-info" value="Back to tours" onclick="location.href='/active_tours'">
        <input type="submit" class="btn btn-info" value="Log Out" align="right">
    </form:form>
    <hr/>
    <h1>All my tours:</h1>
    <%
        List<Tour> tours = null;
        if (!Objects.isNull(request.getAttribute("agentsTours"))) {
            tours = (List<Tour>) request.getAttribute("agentsTours");
        }
        if (Objects.isNull(tours) || tours.isEmpty()) {
            out.print("No tours yet");
        } else {
            out.println("<table class=\"table table-striped table-bordered table-info\">");
            out.println("<tr>");
            out.println("<th>Type</th>");
            out.println("<th>Tour from date</th>");
            out.println("<th>Duration</th>");
            out.println("<th>Hot discount</th>");
            out.println("<th>Discount for regular buyers</th>");
            out.println("<th>Base price</th>");
            out.println("<th>Max amount of people</th>");
            out.println("<th>Place</th>");
            out.println("<th>Description</th>");
            out.println("<th>Active/Hot</th>");
            out.println("</td>");
            for (Tour tour : tours) {
                out.println("<tr>");
                out.println("<td>" + tour.getType().getName() + "</td>");
                out.println("<td>" + tour.getFromDate() + "</td>");
                out.println("<td>" + tour.getDuration() + "</td>");
                out.println("<td>" + tour.getHotDiscount() + "</td>");
                out.println("<td>" + tour.getDiscount() + "</td>");
                out.println("<td>" + tour.getPrice() + "</td>");
                out.println("<td>" + tour.getMaxAmountOfPeople() + "</td>");
                out.println("<td>" + tour.getPlace() + "</td>");
                out.println("<td>" + tour.getDescription() + "</td>");
                out.println("<td>");
                if(tour.isActive()){
                    out.println("Active <br>");
                }else{
                    out.println("Inactive <br>");
                }

                if(tour.isHot()){
                    out.println("Hot <br>");
                }else{
                    out.println("Usual <br>");
                }
                out.println("</td>");
            %>
            <td>
            <%--@elvariable id="tourId" type="java.lang.Long"--%>
            <form:form action="/tour_active_hot_change" method="post">
                <input type="hidden" name="tourId" value=<%=tour.getTourId()%>>
                <input type="submit" class="btn btn-info btn-sm" value="Change Active/Hot">
            </form:form>
            </td>
<%
            out.println("</tr>");
        }
    }
%>

</body>
</html>