<%@ page import="com.epam.studentproject.entity.User" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User info</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid">
    <hr/>
        <input type="button" class="btn btn-info" style="margin: 5px 5px 5px" value="Back to all users" onclick="location.href='/users'">
    <hr/>
    <div align="center">
            <%User user = (User) request.getAttribute("user");%>
        <table class="table table-hover" align="center">
            <tr>
                <td>First name: </td>
                <td><%=user.getFirstName()%></td>
            </tr>
            <tr>
                <td>Last name: </td>
                <td><%=user.getLastName()%></td>
            </tr>
            <tr>
                <td>Role: </td>
                <td><%=user.getRole().getAuthority()%></td>
            </tr>
            <%if(user.getRole().getAuthority().equals("customer")) {%>
            <tr>
                <td>You are <%=!user.isRegular()?"not":""%> regular customer</td>
                <td></td>
            </tr>
            <%}%>
            <tr>
                <td>Email: </td>
                <td><%=user.getEmail()%></td>
            </tr>
            <tr>
                <td>Telephone: </td>
                <td><%=user.getTelephone()%></td>
            </tr>
        </table>

    <%--@elvariable id="userId" type="java.lang.Long"--%>
    <form:form action="/user_info_change" modelAttribute="userId" method="get">
        <input type="hidden" name="userId" value=<%=request.getAttribute("userId")%>>
        <input type="submit" class="btn btn-info" value="Change info" align="center">
    </form:form>
    </div>
</div>
</body>
</html>