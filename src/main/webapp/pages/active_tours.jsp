<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.epam.studentproject.entity.Tour" %>
<%@ page import="com.epam.studentproject.entity.User" %>
<%@ page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Active tours</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

<div class="container-fluid">
    <%
        String userEmail = request.getRemoteUser();
        List<User> users = new ArrayList<>();
        User user = new User();
        if (!Objects.isNull(request.getAttribute("users"))) {
            users = (List<User>) request.getAttribute("users");
            for (User curUser : users) {
                if (curUser.getEmail().equals(userEmail)) {
                    user = curUser;
                    break;
                }
            }
        }
    %>

    <hr/>
    <%if (!Objects.isNull(request.getUserPrincipal())) {%>
    <div class="btn-group" style="margin: 5px 5px 5px">
        <%}%>
        <%
            //customers orders
            if (!Objects.isNull(request.getUserPrincipal())) {
                if ("customer".equalsIgnoreCase(((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority())) {
        %>
        <%--@elvariable id="customerId" type="java.lang.String"--%>
        <form:form action="/customers_orders" modelAttribute="customerId" method="post">
            <input type="hidden" name="customerId" value=<%=user.getUserId()%>>
            <input type="submit" class="btn btn-info" value="My Orders">
        </form:form>
        <%
                }
            }
        %>

        <%
            // add tour button
            if (!Objects.isNull(request.getUserPrincipal())) {
                if ("agent".equalsIgnoreCase(((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority())) {
                    out.println("<input type=\"button\" class=\"btn btn-info\" value=\"Add tour\" onclick=\"location.href='/add_tour'\">");
                }
            }
        %>

        <%
            // all my tours (agent)
            if (!Objects.isNull(request.getUserPrincipal())) {
                if ("agent".equalsIgnoreCase(((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority())) {
                    out.println("<input type=\"button\" class=\"btn btn-info\" value=\"My tours\" onclick=\"location.href='/all_agents_tours'\">");
                }
            }
        %>

        <%
            //agents orders
            if (!Objects.isNull(request.getUserPrincipal())) {
                if ("agent".equalsIgnoreCase(((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority())) {
        %>
        <%--@elvariable id="agentId" type="java.lang.String"--%>
        <form:form action="/agents_orders" modelAttribute="agentId" method="post">
            <input type="hidden" name="agentId" value=<%=user.getUserId()%>>
            <input type="submit" class="btn btn-info" value="My Orders">
        </form:form>
        <%
                }
            }
        %>

        <%
            //inactive tours (for admin)
            if (!Objects.isNull(request.getUserPrincipal())) {
                if ("admin".equalsIgnoreCase(((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority())) {
        %>
        <input type="button" class="btn btn-info" value="All inactive tours" onclick='location.href="/inactive_tours"'>
        <%
                }
            }
        %>

        <%
            //all orders (for admin)
            if (!Objects.isNull(request.getUserPrincipal())) {
                if ("admin".equalsIgnoreCase(((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority())) {
        %>
        <input type="button" class="btn btn-info" value="All Orders" onclick='location.href="/orders"'>
        <%
                }
            }
        %>

        <%
            // all users button (for admin)
            if (!Objects.isNull(request.getUserPrincipal()) && "admin".equalsIgnoreCase(((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities().iterator().next().getAuthority())) {
                out.println("<input type=\"button\" class=\"btn btn-info\" value=\"All users\" onclick=\"location.href='/users'\">");
            }
        %>
        <%if (!Objects.isNull(request.getUserPrincipal())) {%>
    </div>
    <%}%>

    <div class="btn-group" style="margin: 5px 5px 5px">
        <%
            // log in button
            if (Objects.isNull(request.getUserPrincipal())) {
                out.println("<form action=\"/login\">\n" +
                        "    <button type=\"submit\" class=\"btn btn-info\">Sing In</button>\n" +
                        "</form>");
            }
        %>
        <%
            // my info button
            if (!Objects.isNull(request.getUserPrincipal())) {
                out.println("<input type=\"button\" class=\"btn btn-info\" value=\"My Info\" onclick=\"location.href='/my_info'\">");
            }
        %>
        <%
            // log out button
            if (!Objects.isNull(request.getUserPrincipal())) {
        %>
        <form:form action="/logout" method="post">
            <input type="submit" class="btn btn-info" value="Log Out">
        </form:form>
        <%}%>
    </div>
    <hr/>

    <div class="btn-group" style="margin: 5px 5px 5px">
        <input type="button" class="btn btn-info" value="Active tours" onclick="location.href='/active_tours'">
        <input type="button" class="btn btn-info" value="Hot tours" onclick="location.href='/active_tours?hot=1'">
    </div>
    <br>

    <h1>Active tours:</h1>
    <%

        List<Tour> tours = null;
        if (!Objects.isNull(request.getAttribute("active_tours"))) {
            tours = (List<Tour>) request.getAttribute("active_tours");
        }
        if (Objects.isNull(tours) || tours.isEmpty()) {
            out.print("No tours yet");
        } else {
            out.println("<table class=\"table table-striped table-bordered table-info\">");
            out.println("<tr>");
            out.println("<th>Type</th>");
            out.println("<th><label>Tour from date</label></th>");
            out.println("<th>Duration</th>");
            out.println("<th>Is hot</th>");
            out.println("<th>Discount if tour is hot</th>");
            out.println("<th>Discount for regular buyers</th>");
            out.println("<th>Base price</th>");
            out.println("<th>Tours quantity</th>");
            out.println("<th>Place</th>");
            out.println("<th>Description</th>");
            if (request.getRemoteUser() == null || !user.getRole().getAuthority().equals("agent") && !user.getRole().getAuthority().equals("admin")) {
                out.println("<th>To order</th>");
            }
            out.println("</tr>");
            for (Tour tour : tours) {
                if ("1".equals(request.getParameter("hot"))) {
                    if (!tour.isHot()) {
                        continue;
                    }
                }
                out.println("<tr>");
                out.println("<td>" + tour.getType().getName() + "</td>");
                out.println("<td>" + tour.getFromDate() + "</td>");
                out.println("<td>" + tour.getDuration() + "</td>");
                out.println("<td>");
                if (tour.isHot()) {
                    out.println("Hot");
                } else {
                    out.println("Usual");
                }
                out.println("</td>");
                out.println("<td>" + tour.getHotDiscount() + "</td>");
                out.println("<td>" + tour.getDiscount() + "</td>");
                out.println("<td>" + tour.getPrice() + "</td>");
                out.println("<td>" + tour.getMaxAmountOfPeople() + "</td>");
                out.println("<td>" + tour.getPlace() + "</td>");
                out.println("<td>" + tour.getDescription() + "</td>");
    %>

    <% if (request.getRemoteUser() == null || !user.getRole().getAuthority().equals("agent") && !user.getRole().getAuthority().equals("admin")) { %>
    <td>
        <%--@elvariable id="tourId" type="java.lang.String"--%>
        <%--@elvariable id="amount_of_people" type="java.lang.Long"--%>
        <%--@elvariable id="userId" type="java.lang.String"--%>
        <form:form action="/create_order" modelAttribute="tourId" method="post">
            <input type="hidden" name="tourId" value=<%=tour.getTourId()%>>
            <input type="hidden" name="amount_of_people" value=<%=0%>>
            <input type="hidden" name="userId" value=<%=user.getUserId()%>>
            <input type="submit" class="btn btn-info btn-sm" value="Order tour">
        </form:form>
    </td>
    <%}%>
    <%
                out.println("</tr>");
            }
        }
    %>
</div>

</body>
</html>