<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken" %>
<%@page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Hello World!</title>
</head>
<body>
    <h1>Hello <%=request.getRemoteUser()%> <%=((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getAuthorities()%>!</h1>
    <form:form action="/logout" method="post">
        <input type="submit" value="Sign Out"/>
    </form:form>
    <input type="button" value="View all active tours" onclick='location.href="/active_tours"'/>
</body>
</html>