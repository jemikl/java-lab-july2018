<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>
<%@ page import="com.epam.studentproject.dto.OrderDto" %>
<%@ page import="com.epam.studentproject.entity.User" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Orders</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

<hr/>
<div class="container-fluid" style="margin: 5px 5px 5px">
    <form:form action="/logout" method="post">
        <input type="button" class="btn btn-info" value="Back to tours" onclick="location.href='/active_tours'">
        <input type="submit" class="btn btn-info" value="Log Out" align="right">
    </form:form>
<hr/>
    <h1>All orders:</h1>
<%
    String userEmail = request.getRemoteUser();
    List<User> users = new ArrayList<>();
    User user = new User();
    if (!Objects.isNull(request.getAttribute("users"))) {
        users = (List<User>) request.getAttribute("users");
        for (User curUser : users) {
            if (curUser.getEmail().equals(userEmail)) {
                user = curUser;
                break;
            }
        }
    }
%>

<%

    List<OrderDto> orders = null;
    if (!Objects.isNull(request.getAttribute("orders"))) {
        orders = (List<OrderDto>) request.getAttribute("orders");
    }

    if (Objects.isNull(orders) || orders.isEmpty()) {
        out.print("No orders yet");
    } else {
        out.println("<table class=\"table table-striped table-bordered table-info\">");
        out.println("<tr>");
        out.println("<th>Buyer info</th>");
        out.println("<th>Agent info</th>");
        out.println("<th>Orders start date</th>");
        out.println("<th>Order date</th>");
        out.println("<th>Tours quantity bought</th>");
        out.println("<th>Place</th>");
        out.println("<th>Type</th>");
        out.println("<th>Price</th>");
        out.println("</td>");
        for (OrderDto order : orders) {
            out.println("<tr>");
            out.println("<td>" + order.getCustomerFirstName() + " " + order.getCustomerLastName() + "</td>");
            out.println("<td>" + order.getAgentFirstName() + " " + order.getAgentLastName() + "</td>");
            out.println("<td>" + order.getToursStartDate() + "</td>");
            out.println("<td>" + order.getDate() + "</td>");
            out.println("<td>" + order.getAmountOfPeople() + "</td>");
            out.println("<td>" + order.getTourPlace() + "</td>");
            out.println("<td>" + order.getTourTypeName() + "</td>");
            out.println("<td>" + order.getFinalPrice() + "</td>");
%>
<% if (request.getRemoteUser() == null || !user.getRole().getAuthority().equals("agent") && !user.getRole().getAuthority().equals("admin")) { %>
<td>
    <%--@elvariable id="orderId" type="java.lang.Long"--%>
    <form:form action="/delete_order" modelAttribute="orderId" method="post">
        <input type="hidden" name="orderId" value=<%=order.getOrderId()%>>
        <input type="submit" class="btn btn-info btn-sm" value="Delete Order">
    </form:form>
</td>
<%
            }
            out.println("</tr>");
        }
    }
%>
</div>

</body>
</html>